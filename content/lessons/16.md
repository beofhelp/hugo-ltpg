---
title: "Common Beginner Mistakes"
date: 2019-03-31T10:05:17+02:00
draft: false
cookieSetting: "16"
puzzles:
- id: pzl1
  target: 16-1
  text: Overconcentration
- id: pzl2
  target: 16-2
  text: Atari EVERYTHING
- id: pzl3
  target: 16-3
  text: Reluctance to sacrifice
- id: pzl4
  target: 16-4
  text: Help your opponent
returnTo: "lessons/17"
section: "index.html#strategies"
---

# | Most Common Beginner Mistakes
## Little pieces of quick advice

> There is nothing true 100% of the time in Go and a lot of things you "learn" not to do can later turn out to be playable in the right situation and vice versa. I urge you take every advice you can get but not blindly. Use any advice of a stronger player but think about why and when.   

**Every player is different and yet a lot of the mistakes repeat. A clever man learns from the mistakes of others, so check out what I did wrong when I was starting**

{{< sgf >}}

**Urgent before big** - always make sure your groups are safe. Only then look for the biggest move

**Just because it puts a stone in atari does not mean it is a good move**

**Not every stone is worth saving** - a captured stone is worth 1 point. Possibly +1 for the territory. If the stone is not important (not severely cutting anything or needed for life) just sacrifice it. Surely you can think of a move bigger than 2 points.

**Do not try to kill EVERYTHING** - do not be too greedy. If you attack everything it usually means you are also leaving weaknesses everywhere and your whole position may crumble. You only need to get a little more than your opponent everytime.

**Do not be a puppy** - do not just follow your opponent around answering everything he/she does. Look for a time to take the initiative and try to keep it.

**Learn to sacrifice something small to get something bigger elsewhere.**

**In the land of the blind, the  one-eyed man is King ;)**

**If you lost, find out why** - from lost games you can learn the most.

**A bad move is a punishement in itself.**