---
title: "Passing & Counting"
date: 2019-03-31T10:05:17+02:00
draft: false
cookieSetting: "5"
puzzles:
- id: pzl1
  target: 5-1
  text: Puzzle 1
returnTo: "index.html#rules"
section: "index.html#rules"
---

# | Passing & Counting
## How to score a game

> This is the last stop. The last thing preventing you from playing a complete game of Go. I am proud of you. It may seem insignificant but you are THIS close to learning possibly the most complex game the human race knows. You are not too good at playing it just yet, but at least you almost know the rules...

**The end to the game of Go is not as straightforward as say, Chequers or Chess. The game ends when both players feel there is nothing more to gain. Then the player with more points wins (duh).**

![counting diagram](/images/points.jpg)

{{< alert >}}
	⬤ <b>Empty</b> intersections <b>fully</b> surrounded by stones of your color are worth 1 point each.<br>
	⬤ <b>Captured</b> stones and stones in your territorry that <b>cannot avoid capture</b> are worth 1 point each.<br>	
{{< /alert >}}

{{< rule >}}
	Each player has the right to {{< black "pass">}} (literally say pass) instead of placing a stone.
{{< /rule >}}

{{< rule >}}
	If both players pass {{< black "consecutively">}}, the game is over, and we count the score.
{{< /rule >}}

Yes, you got it right. The game can only end when neither player wants to play further.
That sounds easy but it is also easy to screw it up. So, before you pass you should make sure that the game is actually over. That means there is no move that could bring any points. Most notably: 

{{< tsumego >}}

After the game ends (two consecutive passes) we first remove all stones that have no way of preventing capture and add them as prisoners (just to save the time of playing out every needless move). This may sound confusing at first but it really isn't that complicated. See the example below - let's say both players passed in this position. 

![counting diagram](/images/counting.jpg)

Any seasoned go player would know that the lone black stone has no chance of preventing capture. If you were not sure, you simply would not pass. It would make no sense to pass in this position if you thought that black stone might survive because in such a case borders around the respective territories would not be finished yet. 

{{< rule >}}
	You {{< black "do not">}} have to capture every last dead stone in your territory. If it cannot live it is counted as a prisoner anyway once we start scoring.
{{< /rule >}}

{{< rule >}}
	Playing inside your own territory when you do not need {{< black "will cost you">}} a point (only empty intersections count for points).
{{< /rule >}}

Now we only count the empty spaces that are fully surrounded by stones of one color. Then add the prisoners and that's it. Luckily for us who play online, the computer counts it for us. :) If you want to find out where you can now play online, check <u>[here](../18)</u>.

{{< extra "1" "How to better recognize dead stones" >}}
To help you recognize dead stones until you gain more experience, here are some tips on how dead stones look:
<br><br> 
<b>⬤  It is only a handful of stones (below 5)</b><br>
<b>⬤  They are not surrounding any territory (that is a huge tell!)</b><br>
<b>⬤  They cannot hope to capture anything (surrounded by well-connected enemy stones)</b><br>

But I repeat; If you are not sure, do not pass.
{{< /extra >}}

{{< extra "2" "Example of a finished game" >}}
<br><img src="/images/counting2.jpg" alt="counting diagram""><br>

Assuming komi (a compensation white gets for black's first move) white wins by 1.5 points (24 points of territory +1 for the prisoner +5.5 for komi against black's 29). In Go the result is determined by the difference between the two scores, not the actual number of points. That one space in the middle? It does not count for either color. Remember only territory fully surrounded by **one** color can be counted. 
{{< /extra >}}

{{< extra "3" "Different rulesets in Go" >}}
And here comes the curveball but I had to tell you sooner or later. These things I just taught you apply for Japanese scoring... Yeah. Go has several rulesets and Japanese are arguably the most popular in Western Europe. Does that mean you cannot play with someone who was taught Chinese scoring? Luckily, no! All the basic principles are the same and unless you played something very weird the result will be the same as well. So, it almost does not matter what rules you know. If I were you, I would not bother with the differences right now.
If you are really, really curious about the differences, you can check them out {{< externalLink target="../rulesets" name="here" >}}.
{{< /extra >}}