---
title: "Seki"
date: 2019-03-31T10:05:17+02:00
draft: false
cookieSetting: "12"
puzzles:
- id: pzl1
  target: 12plus-1
  text: Puzzle 1
- id: pzl2
  target: 12plus-2
  text: Puzzle 2
returnTo: "index.html#techniques"
section: "index.html#techniques"
---

# | Playing Under the Stones
## What a weird concept

> This is one of those special techniques that actually comes into play like once a year, at most. But man, when you get it right, you're on the top of the world!  

"Playing under the stones" actually means purposefully sacrificing a small group to get enough space to play there again.
 Sounds confusing? Well the whole technique is quite confusing. Hopefully you will understand it when you see it. 
 
{{< tsumego >}}

These are super advanced tricks and you have to see them way in advance to set them up. I would not worry too much about this actual technique. It was just a demonstration of how deep Go is and how you should, 'Expect the unexpected', as you discover Go...but you can't deny the coolness factor, can you? 