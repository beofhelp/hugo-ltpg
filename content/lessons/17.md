---
title: "Commented Professional Game of Go"
date: 2019-03-31T10:05:17+02:00
draft: false
cookieSetting: "17"
puzzles:
- id: pzl1
  target: 17-1
  text: A professional game
returnTo: "index.html#strategies"
section: "index.html#strategies"
---

# | Understand a Pro Game
## Can you even?

> For most of us mere mortals, professional strength is something quite unobtainable but that does not mean we cannot learn from them!    

**First thing to realize before observing a professional game is that you will not understand everything. You are not even supposed to. If you did you would be a Pro!**

Some choices and moves are simply based on a reading ability way beyond ours. If you cannot see the full consequences of a move, you cannot fully understand it. That's okay. What you should be able to understand and strive to incorporate into your games, is the self-discipline. Especially Japanese pros will often play "simple" moves. They will have balance. They attack a bit and then they will defend their own weakness. Where we would play a complicated fancy shape to "do more than just protect" pros will choose a simple solid connection. 
 
*Calm solid moves. A pressure move then a defensive move. An attack, a defense. Like a wavy ocean or a dance. Go forward, go back. Simple, beautiful, calm.*

Let's see how much we can understand. I am sure my analysis is not perfect, but neither will yours be. We are amateurs. Do not look for answers, look for inspiration. **Look how the pros are NOT greedy at all**. You will never see a move saying THIS IS ALL MINE! Professionals know that the opponent is entitled to points too.

{{< sgf >}}