---
title: "No Suicide Rule"
date: 2019-03-31T10:05:17+02:00
draft: false
cookieSetting: "3"
returnTo: "lessons/04"
section: "index.html#rules"
puzzles:
- id: pzl1
  target: 3-1
  text: Puzzle 1
- id: pzl2
  target: 3-2
  text: Puzzle 2
- id: pzl3
  target: 3-3
  text: Puzzle 3
---

# | There Are No Suicides in Go
## Just ruthless captures and willing sacrifices. How nice.

> This rule is very easy and quite logical but it needs to be mentioned. Soon you will play a real game!

**It's simple. You must not kill your own stones and there is a reason I am even telling you this...**

{{< rule >}}
    A stone that would have {{< black "no liberties">}} (or fill the last liberty of your group) must not be placed.
{{< /rule >}}

{{< rule >}}
    {{< black "Unless" >}} the very same move captures something. In that case it is allowed (because the capture gives you at least one liberty).
{{< /rule >}}

{{< tsumego >}}

{{< alert >}}
    Since there can never be a stone without a liberty, it is possible to create an "immortal" group but don't worry if you can't imagine how right now. It is a topic for later.
{{< /alert >}}