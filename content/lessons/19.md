---
title: "Where to Learn Go"
date: 2019-03-31T10:05:17+02:00
draft: false
layout: info
returnTo: "index.html#what_now"
section: "index.html#what_now"
---

# | Where Can I Learn More?
## Not done learning yet?

> You will never be done learning.  

**Go wiki**

{{< rule >}}
	<a href="https://senseis.xmp.net/" target="_blank">www.senseis.xmp.net</a>  
{{< /rule >}}

This is a Go player's Wikipedia. It is not perfect but it is the best we have and is packed with information.
<br><br> 

**Online Go Clubs**
{{< rule >}}
	<a href="https://www.openstudyroom.org" target="_blank">Open Study Room</a>  
{{< /rule >}}
An amazing project run by a dear friend of mine. It is basically a Go club online. Lectures and reviews by high-dan or even professional players, league games for prizes, lots of friendly advice...
<br><br>

**Video Lectures (there are a lot to choose from, so just to cover the basics...)**
{{< rule >}}
	<a href="https://www.youtube.com/user/nicksibicky" target="_blank">Nick Sibicky</a>  
{{< /rule >}}

Great guy, publishing actual lectures from an American Go club. I think these are great for beginners. 

{{< rule >}}
	<a href="https://www.youtube.com/user/dwyrin" target="_blank">Dwyrin</a>  
{{< /rule >}}

A famous high dan player with highly regarded "Basic series" which focuses on the fundamentals. Might be good knowing a bit already before watching.

{{< rule >}}
	<a href="https://www.youtube.com/user/USGOWeb" target="_blank">Official AGA channel</a>  
{{< /rule >}}

Probably for those at least around Single digit kyu level. Hosting reviews by the absolutely phenomenal Micheal Redmond (9p).

{{< rule >}}
	<a href="https://www.youtube.com/user/sundaygolessons" target="_blank">Sunday Go Lessons</a>  
{{< /rule >}}

Posting translated commentaries from Japanese title matches. Go watch Cho Chikun, it is a treat.

**Series and Movies**
{{< rule >}}
	<a href="https://www.imdb.com/title/tt0426711/" target="_blank">Hikaru no Go</a>  
{{< /rule >}}
Is a beloved animated series about a young Go player on his way to professional level. With huge attention to detail and a great selection of actual games, all levels can learn something from watching.
 