---
title: "Learn to Play Go"
date: 2019-04-20T10:05:17+02:00
draft: false
layout: test
---

<!-- BASIC RULES -->
  {{< wrapper id="rules" title="| Basic rules" subtitle="Just 5 simple steps" hidden="1" >}}
    {{< progressbar id="progressbar" onclick="learn()" >}}Learn them {{< black all >}} one by one{{< /progressbar >}}

      {{< buttonBlue target="/lessons/01/" id="1" img="/images/where.png" >}}{{< black WHERE >}} and {{< black HOW >}} we place the stones{{< /buttonBlue >}}
      {{< buttonBlue target="/lessons/02/" id="2" img="/images/atari.png" >}}How can we {{< black CAPTURE >}} something?{{< /buttonBlue >}}
      {{< buttonBlue target="/lessons/03/" id="3" img="/images/suicide.png" >}}No {{< black SUICIDES >}} please{{< /buttonBlue >}}
      {{< buttonBlue target="/lessons/04/" id="4" img="/images/endless.png" >}}No {{< black "ENDLESS REPETITION">}} either{{< /buttonBlue >}}
      {{< buttonBlue target="/lessons/05/" id="5" img="/images/pass.png" >}}{{< black PASS >}} and {{< black COUNT >}} the score{{< /buttonBlue >}}
	  
	  {{< buttonQuizz target="/quizzes/1/" id="q1" >}}Bonus quiz{{< /buttonBlue >}}
  {{< /wrapper >}}
<!-- END OF BASIC RULES -->


<!-- BASIC TECHNIQUES -->
  {{< wrapper id="techniques" title="| Basic (but cool!) techniques" subtitle="Played some games already? Now learn to play with style!" hidden="1" >}}

    {{< progressbar id="progressbar2" onclick="learn2()" >}}Learn them {{< black all >}} one by one{{< /progressbar >}}    
      {{< buttonBlue target="/lessons/06/" id="6" img="/images/alive.png" >}}Alive {{< black "(IMMORTAL)" >}} groups{{< /buttonBlue >}}
      {{< buttonBlue target="/lessons/07/" id="7" img="/images/cuts.png" >}}{{< black CUTS >}}{{< /buttonBlue >}}
      {{< buttonBlue target="/lessons/08/" id="8" img="/images/double.png" >}}{{< black DOUBLE >}} atari!{{< /buttonBlue >}}
      {{< buttonBlue target="/lessons/09/" id="9" img="/images/ladder.png" >}}{{< black LADDERS >}}{{< /buttonBlue >}}
      {{< buttonBlue target="/lessons/10/" id="10" img="/images/net.png" >}}{{< black NETS >}}{{< /buttonBlue >}}
      {{< buttonBlue target="/lessons/11/" id="11" img="/images/snapback.png" >}}{{< black SNAPBACK >}}{{< /buttonBlue >}}
      {{< buttonBlue target="/lessons/12/" id="12" img="/images/seki.png" >}}{{< black "SEKI (barely alive)" >}}{{< /buttonBlue >}}
      {{< buttonBlue target="/lessons/12_and_a_half/" id="gold" img="/images/under.png" extraCSS="hidden" inlineCSS="display: none;" >}}Playing {{< black UNDER >}} the stones{{< /buttonBlue >}}
  {{< /wrapper >}}
<!-- END OF BASIC TECHNIQUES -->

<!-- BASIC STRATEGIES -->
  {{< wrapper id="strategies" title="| Basic strategies" subtitle="For those who feel like switching to 19x19" hidden="1" >}}

    {{< progressbar id="progressbar3" onclick="learn3()" >}}Learn them {{< black all >}} one by one{{< /progressbar >}}

    {{< buttonBlue target="/lessons/13/" id="13" img="/images/opening.png" >}}What to do in the {{< black OPENING >}}{{< /buttonBlue >}}
    {{< buttonBlue target="/lessons/14/" id="14" img="/images/endgame.png" >}}Is {{< black ENDGAME >}} important?{{< /buttonBlue >}}
    {{< buttonBlue target="/lessons/15/" id="15" img="/images/shape.png" >}}Make some {{< black SHAPE >}}{{< /buttonBlue >}}
    {{< buttonBlue target="/lessons/16/" id="16" img="/images/mistake.png" >}}Most common beginner {{< black MISTAKES >}}{{< /buttonBlue >}}
    {{< buttonBlue target="/lessons/17/" id="17" img="/images/professional.png" >}}Understand a {{< black PROFESSIONAL >}} game{{< /buttonBlue >}}
      
  {{< /wrapper >}}
<!-- END OF BASIC STRATEGIES -->


<!-- WHAT NOW? -->
  {{< wrapper id="what_now" title="| What's next?" subtitle="Where can you use your newfound skill?" >}}

    {{< buttonBlue target="/lessons/18/" id="18" img="/images/play.png" >}}Where can you {{< black PLAY >}}?{{< /buttonBlue >}}
    {{< buttonBlue target="/lessons/19/" id="19" img="/images/learn.png" >}}Where can you {{< black LEARN >}} more?{{< /buttonBlue >}}
    {{< buttonBlue target="/lessons/20/" id="20" img="/images/ask.png" >}}Where can you {{< black "ASK QUESTIONS" >}}?{{< /buttonBlue >}}
    {{< buttonBlue target="/lessons/21/" id="21" img="/images/dictionary.png" >}}{{< black DICTIONARY >}} of Go terms?{{< /buttonBlue >}}
      
  {{< /wrapper >}}
  
  
<!-- CREDITS -->
  {{< wrapper id="credits" title="| Who's behind this?"  >}}
  
  This site was made for the love of the game and our community. If you found a mistake contact {{< externalLink target="https://online-go.com/user/view/360861" name="AdamR" >}} on the OGS, I will try to fix it as soon as possible.

My sincerest thanks to all the members of the OGS community who were very supportive in my efforts and kept trying to make it as good as possible. Most notably:<br><br> 

{{< black "Lucas Westermann" >}}<br>
Who managed the whole technical side of the project and patiently kept explaining what I need to press to make it all work.
	<br><br>
{{< black "Kosh " >}}(faith manages)<br> 
Who wrote a portion of the texts, spent his evenings crawling through heaps of my horrible grammar and overall lifted the quality of the site monumentally.
	<br><br>
{{< black "Joshua Batson" >}}<br> 
Who also helped immensely with the texts, and was an endless source of inspiration and ideas for new features.
    
  {{< /wrapper >}}  
